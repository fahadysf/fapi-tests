from typing import Union
from fastapi import FastAPI

import os
import socket
myhost = os.uname()[1]
ip = (socket.gethostbyname(socket.gethostname()))
data = {}
app = FastAPI()


@app.on_event('startup')
def init_data():
    print("init call")
    data["counter"] = 0
    return data


@app.get("/")
def read_root():
    """
    Just dumps out the hostname and the ip of the pod where 
    this container is running.
    """
    data["counter"] = data["counter"] + 1
    return {
        "pod": myhost,
        "ip": ip,
        "counter": data["counter"]
    }
